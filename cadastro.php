<?
$nome = ($_SESSION['cadastro'][$_GET['id']]['nome']) ? $_SESSION['cadastro'][$_GET['id']]['nome'] : '';
$cpf = ($_SESSION['cadastro'][$_GET['id']]['cpf']) ? $_SESSION['cadastro'][$_GET['id']]['cpf'] : '';
$email = ($_SESSION['cadastro'][$_GET['id']]['email']) ? $_SESSION['cadastro'][$_GET['id']]['email'] : '';
$telefone = ($_SESSION['cadastro'][$_GET['id']]['telefone']) ? $_SESSION['cadastro'][$_GET['id']]['telefone'] : '';

?>
<h3 class="text-center mt-5" >Cadastrar Clientes</h3>
<form action="#" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="nome">Nome completo</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="d[nome]" id="nome" value="<?=$nome?>" required>
        </div>
        <div class="form-group col-md-6">
            <label for="cpf">CPF/CNPJ</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="d[cpf]" id="cpf" value="<?=$cpf?>" required>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="email">E-mail</label>
            <input type="email" class="form-control shadow mb-3 bg-white rounded" name="d[email]" id="email" value="<?=$email?>" required>
        </div>
        <div class="form-group col-md-4">
            <label for="tel">Telefone/Celular</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="d[telefone]" value="<?=$telefone?>" id="tel">
        </div>
    </div>

    <? if($_GET['id']){ ?>
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <? } ?>
    
    <button type="submit" class="btn btn-dark">Cadastrar</button>
</form>