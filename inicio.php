<h1 class="text-center my-5">Gestão de Clientes</h1>
<div class="row">
    <div class="col-12 col-md-6 mb-4">
        <div class="card mx-auto shadow bg-white rounded" style="width: 18rem;">
            <i class="bi bi-person-plus-fill text-center" style="font-size: 80px;"></i>
            <div class="card-body">
                <h5 class="card-title">Cadastrar novo Cliente</h5>
                <p class="card-text text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, explicabo. Est ipsum culpa maxime libero quibusdam.</p>
                <a href="?page=cadastro" class="btn btn-dark">Cadastrar</a>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-6 mb-4">
        <div class="card mx-auto shadow bg-white rounded" style="width: 18rem;">
            <i class="bi bi-card-list text-center" style="font-size: 80px;"></i>
            <div class="card-body">
                <h5 class="card-title">Listagem de Cadastros</h5>
                <p class="card-text text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, explicabo. Est ipsum culpa maxime libero quibusdam.</p>
                <a href="?page=listagem" class="btn btn-dark">Listagem</a>
            </div>
        </div>
    </div>
</div>