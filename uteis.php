<?
session_start();
error_reporting('E_FATAL | E_PARSE');

    $nav = array(
        'inicio' => 'Home',
        'cadastro' => 'Cadastro',
        'listagem' => 'Listagem'
    );

function dateFormat($d, $tipo = true){ //2022-03-23 16:24:37
    
    if (!$d) {
        return "Não editado";
    }
    if ($tipo) {
        $hora = explode(' ',$d);
        $data = explode('-',$hora[0]);
        return $data[2].'/'.$data[1].'/'.$data[0].' '.$hora[1];
    } else {
        $hora = explode(' ',$d);
        $data = explode('/',$hora[0]);
        return $data[2].'-'.$data[1].'/'.$data[0].' '.$hora[1];
    }
}

// session_destroy();
?>