$(function () {
    $("form").submit(function () {
        var nome = $(this).find('input[name="d[nome]"]').val();
        var cpf = $(this).find('input[name="d[cpf]"]').val();
        var email = $(this).find('input[name="d[email]"]').val();
        var telefone = $(this).find('input[name="d[telefone]"]').val();
        var editar = $(this).find('input[name="editar"]').val();
       
        var url;
        if(editar){
            url = 'api/editaCliente.php';
        } else{
            url = 'api/cadastraCliente.php';
        }

        $.ajax({
            url: url,
            dataType: 'html',
            type: 'POST',
            data : {
                nome : nome,
                cpf : cpf,
                email : email,
                telefone : telefone,
                editar : editar
            },
            success : function(data){
                // console.log(data);
                if(data == 'cadastrou'){
                    alert('Cadastro Efetuado com sucesso');
                   window.location.href = 'index.php?page=cadastro';
                }else if(data == 'editou'){
                    alert('Cadastro atualizado com sucesso');
                   window.location.href = 'index.php?page=listagem';
                }
            }
        });

        return false;
    });

    $('#listaClientes').on('click','.removerCliente' ,function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: 'api/deletaCliente.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                if(data.status == 'ok'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html(data.totalRegistros);
                }
            }
        })
        return false;
    })
});

