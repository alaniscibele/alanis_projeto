<?
require "../uteis.php";
require "../class/cadastro.Class.php";

$cliente = new Cadastro();
if($cliente->deletaCliente($_POST['id'])){
    
    $totalRegistros = count($_SESSION['cadastro']);

    $result = array(
        "status" => 'ok',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Parabéns, seu registro foi deletado"
    );

    echo json_encode($result);
}
?>