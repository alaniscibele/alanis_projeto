<?
    require "class/cadastro.Class.php";
?>
<h3 class="text-center mt-5 mb-4">Clientes Cadastrados</h3>
<div class="table-responsive">
    <table id="listaClientes" class="table table-striped my-3 table-hover shadow bg-white rounded">
        <tr>
            <th scope="col">Nome Completo</th>
            <th scope="col">CPF/CNPJ</th>
            <th scope="col">E-mail</th>
            <th scope="col">Telefone</th>
            <th scope="col">Data Cadastro</th>
            <th scope="col">Última Edição</th>
            <th scope="col" colspan="2">Ações</th>
        </tr>
            
            <? 
            $cliente = new Cadastro();
            foreach ($cliente->getClientes($_SESSION) as $chDados => $dados) { 
            ?>
            <tr data-id="<?=$chDados?>">
                <td><?= $dados['nome'] ?></td>
                <td><?= $dados['cpf'] ?></td>
                <td><?= $dados['email'] ?></td>
                <td><?= $dados['telefone'] ?></td>
                <td><?=dateFormat($dados['dataCadastro'])?></td>
                <td><?=dateFormat($dados['dataUpdate'])?></td>
                <td>
                    <a class="text-dark h4" href="?page=cadastro&id=<?= $chDados; ?>"><i class="bi bi-pencil-square"></i></a>
                    <a class="text-dark h4 removerCliente"  href="#" data-id="<?=$chDados?>"><i class="bi bi-trash3-fill"></i></a>
                <td>
            </tr>
        <? } ?>
    </table>
</div>
<div class="row mb-3">
    <div class="col-6 col-md-6">
        <a href="?page=cadastro" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Cliente</a>
    </div>
    <div class="col-6 col-md-6">
        <p class="text-right ">Total de registros <span class="totalRegistros badge badge-dark"><?= count($_SESSION['cadastro']) ?></span></p>
    </div>
</div>


<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['cadastro'][$_GET['deletar']]);
    header('Location: index.php?page=listagem');
}
?>