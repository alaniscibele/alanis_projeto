<?
Class Cadastro{
    // public $nome;
    // public $cpf;
    // public $email;
    // public $telefone;
    protected $dados = array();
    protected $id;

    function __construct(){

    }

    function getClientes(){
        return $_SESSION['cadastro'];
    }

    function setCliente($dados){

        $contagem = count($_SESSION['cadastro']) + 1;
        foreach($dados as $ch=>$campos){
            $_SESSION['cadastro'][$contagem][$ch] = $dados[$ch];
        }
        $_SESSION['cadastro'][$contagem]['dataCadastro'] = date('Y-m-d H:i:s');

        return true;
    }

    function editCliente($dados){
        foreach($_SESSION['cadastro'][$dados['editar']] as $ch=>$edit){
            if($ch != 'dataCadastro' && $ch != 'dataUpdate'){
                $_SESSION['cadastro'][$dados['editar']][$ch] = $dados[$ch];
            }
        }   
        $_SESSION['cadastro'][$dados['editar']]['dataUpdate'] = date('Y-m-d H:i:s');

        return true;
    }

    function deletaCliente($id){
        unset ($_SESSION['cadastro'][$id]);
        return true;
    }
}
?>