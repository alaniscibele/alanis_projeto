<?
include "uteis.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página Inicial</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark container-fluid">
    <a class="navbar-brand" href="index.php?page=inicio"><i class="bi bi-briefcase-fill"></i></a>
        <? foreach($nav as $ch=>$menu){?>
            <a class="nav-link text-white" href="index.php?page=<?=$ch?>"><?=$menu?></a>
        <? } ?>
    </nav>


    <main class="container">
        <?
            switch ($_GET['page']) {
                case '':
                case 'inicio':
                    require "inicio.php";
                    break;
                default:
                    require $_GET['page'] . '.php';
                    break;
            }
        ?>
    </main>


<footer class="fixed-bottom">
    <div class="w-100 py-2 px-2 bg-dark text-white tx-small" ><small>&copy; Todos os direitos reservados.</small></div>
</footer>

    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/app.js?v=<?=rand(0,9999)?>"></script>
</body>
</html>